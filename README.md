
# Lady Nora
By TilCreator (tils.pw) and Sonopard
This is our first DIY quadcopter and Ardupilot project, so the main goal is learning from experience.
The wooden frame is very much inspired by [Quadcopter Frame by Lucas Ribeiro](https://grabcad.com/library/quadcopter-frame--1), but the current iteration is drawn from scratch and optimized through FreeCAD FEM / CalculiX.

The internal electronics are:
- A BeagleBone Blue as flight controller and providing telemetry over WiFi.
It is cheaper, more powerful and more available than most flight controllers currently.
- Spektrum Satellite RC receiver.
- Discontinued leftover Atmel based ESCs by Microcopter, given a new life thanks to [tgy](https://github.com/sim-/tgy)
- Hall effect current sensor 
- Sharp IR distance sensor
- Moebius action cam

Nora flies very well, the wooden frame is quite stable, but will break on every crash higher than 2m - speaking from experience -, at least it's easy to replace.

The main motivation for making a laser cut wooden DIY frame is that iteration is fast and cheap, integration of different components can be performed quickly and conveniently.

We've flown multiple successful autopilot missions already.
The next step for the project is integration of a Raspberry Pi Cam on a gimbal based on the open source [BruGi](https://github.com/simonliu009/brushless-gimbal-brugi) controller, to perform aerial terrain scans and 3D scans of buildings through Photogrammetry.
Plans also include cloverleaf WiFi Antennas and a tracking ground station.

